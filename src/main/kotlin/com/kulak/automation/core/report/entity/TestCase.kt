package com.kulak.automation.core.report.entity

data class TestCase(var name: String, var description: String = "", var number: String = "")
