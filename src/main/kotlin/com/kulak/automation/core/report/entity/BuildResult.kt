package com.kulak.automation.core.report.entity

class BuildResult(var status: Status, var passed: Int, var failed: Int, var skipped: Int)
