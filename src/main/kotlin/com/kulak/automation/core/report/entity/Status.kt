package com.kulak.automation.core.report.entity

enum class Status {
    FAILED,
    PASSED
}
