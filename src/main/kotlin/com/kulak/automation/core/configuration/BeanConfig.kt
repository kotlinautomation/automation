package com.kulak.automation.core.configuration

import com.kulak.core.driver.DesktopDriverServiceImpl
import com.kulak.core.driver.DesktopElementServiceImpl
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource("classpath:application.properties")
open class BeanConfig {
    @Bean
    open fun desktopDriverService(@Value("\${core.url}") url: String): DesktopDriverServiceImpl =
        DesktopDriverServiceImpl(url)

    @Bean
    open fun desktopElementService(@Value("\${core.url}") url: String): DesktopElementServiceImpl =
        DesktopElementServiceImpl(url)
}
