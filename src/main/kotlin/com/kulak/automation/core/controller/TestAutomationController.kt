package com.kulak.automation.core.controller

import com.kulak.automation.core.runner.Runner
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1")
class TestAutomationController(@Autowired var runner: Runner) {
    var log: Logger = LoggerFactory.getLogger(TestAutomationController::class.java)

    @GetMapping("/suites")
    fun runSuites(@RequestParam paths: List<String>) {
        log.info("Suite paths: ${paths.joinToString(",")}")
        runner.run(paths)
    }
}
