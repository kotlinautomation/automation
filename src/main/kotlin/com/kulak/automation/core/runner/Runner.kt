package com.kulak.automation.core.runner

// import com.kulak.automation.core.util.ResourceUtils
import com.kulak.automation.core.report.CustomReporter
import com.kulak.automation.core.runner.listener.SuiteListener
import com.kulak.automation.core.runner.listener.TestListener
import java.io.File
import java.util.Date
import java.util.UUID
import org.apache.commons.lang3.time.FastDateFormat
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.testng.TestNG
import org.testng.xml.Parser
import org.testng.xml.XmlSuite

@Service
object Runner {
    private val testNG = TestNG(false)
    private var log: Logger = LoggerFactory.getLogger(Runner::class.java)

    fun run(paths: List<String>) {
        assignListeners()
        parseSuitePaths(paths)
        testNG.outputDirectory = createOutputDirectory()
        testNG.run()
    }

    private fun parseSuitePaths(paths: List<String>) {
        val suites = mutableListOf<XmlSuite>()
        paths.map { "src/main/resources/suites/$it" }
//            .map { ResourceUtils.getResourceAsStream(it) }
            .map { Parser(it) }
            .map { it.parse() }
            .forEach { suites.addAll(it) }
        testNG.setXmlSuites(suites)
        log.info("Suites were parsed.")
    }

    private fun assignListeners() {
        testNG.setListenerClasses(
            listOf(
                SuiteListener::class.java,
                TestListener::class.java,
                CustomReporter::class.java
//                HTMLReporter::class.java
            )
        )
    }

    private fun createOutputDirectory(): String {
        val root = "test-output"
        val today = root + File.separator + FastDateFormat.getInstance("dMy").format(Date())
        val testsDir = today + File.separator + UUID.randomUUID().toString()
        val todayDir = File(today)
        if (!todayDir.exists()) {
            log.info(today)
            todayDir.mkdirs()
        }
        File(testsDir).mkdirs()
        return testsDir
    }
}
