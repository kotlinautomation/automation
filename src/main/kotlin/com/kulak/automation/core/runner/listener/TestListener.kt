package com.kulak.automation.core.runner.listener

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testng.ITestContext
import org.testng.ITestListener
import org.testng.ITestResult

class TestListener : ITestListener {
    private var log: Logger = LoggerFactory.getLogger(TestListener::class.java)

    override fun onFinish(context: ITestContext?) {
        log.info("${context?.name} finished.")
    }

    override fun onTestSkipped(result: ITestResult?) {
        log.info("Test ${result?.name} skipped.")
    }

    override fun onTestSuccess(result: ITestResult?) {
        log.info("Test ${result?.name} passed.")
    }

    override fun onTestFailure(result: ITestResult?) {
        log.info("Test ${result?.name} failed.")
    }

    override fun onTestFailedButWithinSuccessPercentage(result: ITestResult?) {
        log.info("Test ${result?.name} failed but within success percentage.")
    }

    override fun onTestStart(result: ITestResult?) {
        log.info("Test ${result?.name} started.")
    }

    override fun onStart(context: ITestContext?) {
        log.info("${context?.name} started.")
    }
}
