package com.kulak.automation.core.runner.listener

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testng.ISuite
import org.testng.ISuiteListener

class SuiteListener : ISuiteListener {
    private var log: Logger = LoggerFactory.getLogger(SuiteListener::class.java)

    override fun onFinish(suite: ISuite) {
        log.info("Finishing suite ${suite.name}.")
    }

    override fun onStart(suite: ISuite) {
        log.info("Starting suite ${suite.name}.")
    }
}
