package com.kulak.automation.core.util

import java.io.InputStream

class ResourceUtils {
    companion object {
        fun getResourceAsStream(path: String): InputStream =
            ResourceUtils::class.java.classLoader.getResourceAsStream(path)!!
    }
}
