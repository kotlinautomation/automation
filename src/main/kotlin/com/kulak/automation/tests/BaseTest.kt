package com.kulak.automation.tests

import com.kulak.core.constants.BROWSER_NAME
import com.kulak.core.entities.actions.Action
import com.kulak.core.entities.browsers.BrowserType
import org.testng.Assert
import org.testng.annotations.AfterTest
import org.testng.annotations.Test

class BaseTest : EnvironmentSetup() {
    @Test
    fun openBrowser() {
        val result = desktopDriverService.startDriver(
            mapOf(BROWSER_NAME to BrowserType.CHROME.browserName),
            true
        )
        Assert.assertEquals(result.sessionId, "debug")
    }

    @AfterTest
    fun tearDown() = desktopDriverService.getDrivers().let {
        it.forEach { driver ->
            desktopDriverService.quitDriver(
                Action(driver.sessionId)
            )
        }
    }
}
