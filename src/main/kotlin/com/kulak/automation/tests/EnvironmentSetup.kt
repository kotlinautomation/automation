package com.kulak.automation.tests

import com.kulak.automation.core.configuration.BeanConfig
import com.kulak.core.driver.DesktopDriverServiceImpl
import com.kulak.core.driver.DesktopElementServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests

@ContextConfiguration(classes = [BeanConfig::class])
open class EnvironmentSetup : AbstractTestNGSpringContextTests() {
    @Autowired
    @Qualifier("desktopDriverService")
    lateinit var desktopDriverService: DesktopDriverServiceImpl
    @Autowired
    @Qualifier("desktopElementService")
    lateinit var desktopElementService: DesktopElementServiceImpl
}
